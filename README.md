Deprecated
===============

The module has been renamed, see [Views area library](https://www.drupal.org/project/views_area_library).